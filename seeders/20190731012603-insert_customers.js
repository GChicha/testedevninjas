"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "Customers",
      [
        {
          name: "Igor Correia Lima",
          cpf: "72455032736",
          email: "IgorCorreiaLima@dayrep.com"
        },
        {
          name: "Felipe Cardoso Santos",
          cpf: "13145888773",
          email: "FelipeCardosoSantos@rhyta.com"
        },
        {
          name: "Vitór Almeida Barbosa",
          cpf: "37535818870",
          email: "VitorAlmeidaBarbosa@armyspy.com"
        },
        {
          name: "Julia Dias Azevedo",
          cpf: "31646394143",
          email: "JuliaDiasAzevedo@rhyta.com"
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
