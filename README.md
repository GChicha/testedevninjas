# Teste DevNinjas

## O que foi feito

- Todos os endpoints
- Todas regras de negócio foram obedecidas
- Autenticação (Bônus)
- API Rest

## O que não foi feito

- Popular o banco
    - Esperava utilizar o sequelize para isso, mas por algum motivo ainda desconhecido os seeders não funcionaram para mim
- Docker
    - Acredito que exista algum bug com node utilizando modules (Ainda é uma função experimental do Node)
- Documentar a API
    - Embora não era requisito era minha vontade produzir um ```swagger.yml```
- Testes unitarios
    - Não tenho experiencias com testes envolvendo banco de dados. O ORM não facilitou nessa tarefa.

## Instruções para rodar

```bash
npm install
npm run migrate
npm start
```

Para determinar a porta deve-se especificar a variavel de ambiente ```NODE_PORT```

Ainda é possível determinar a URL do banco de dados de produção utilizando a váriavel de ambiente ```DATABASE_URL```, não foi testado o comportamento.

## Algumas considerações

- **Quanto ao ORM**

Deveria ter usado um query builder e não utilizar nenhum ORM. Este somente me atrasou e impos dificuldades desnecessarias, muitas delas por conta da péssima documentação.

- **Linguagem**

Embora mais acostumado ao **python** o **node** é muito melhor na produção de uma API Rest.
Realizei algumas tentativas de utilizar o Flask no lugar do Node, no entanto, a forma que ele
sugere escrever middleware é muito inferior ao Node. Além disso, a comunidade do Node também
é maior que o python nesse segmento.

Python ainda não seria a linguagem que escolheria em um projeto de um backend de uma aplicação.
