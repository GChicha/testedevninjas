import express from "express";
import morgan from "morgan";
import bodyParser from "body-parser";

import ProductRouter from "./routes/product";
import CustomerRouter from "./routes/customer";
import OrderRouter from "./routes/order";
import UserRouter from "./routes/user";

import { isAuthenticated } from "./routes/middleware";

const app = express();

app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/products", isAuthenticated, ProductRouter);
app.use("/customers", isAuthenticated, CustomerRouter);
app.use("/orders", isAuthenticated, OrderRouter);
app.use("/user", UserRouter);
app.use((req, res) => res.sendStatus(404));

const port = process.env.NODE_PORT || 3000;

app.listen(port, () => console.log(`Listening on port ${port}`));
