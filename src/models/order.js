"use strict";
module.exports = (sequelize, DataTypes) => {
  const Order = sequelize.define(
    "Order",
    {
      customer_id: DataTypes.INTEGER,
      status: {
        type: DataTypes.STRING,
        defaultValue: "OPEN",
        validate: {
          is: /OPEN|CONCLUDED|CANCELED/
        }
      }
    },
    {
      getterMethods: {
        total() {
          const items = this.getDataValue("OrderItems") || [];
          return items.reduce((acc, item) => acc + item.subTotal, 0);
        }
      }
    }
  );
  Order.associate = function(models) {
    Order.belongsTo(models.Customer, { foreignKey: "customer_id" });
    Order.hasMany(models.OrderItem, { foreignKey: "order_id" });
  };
  return Order;
};
