"use strict";
module.exports = (sequelize, DataTypes) => {
  const OrderItem = sequelize.define(
    "OrderItem",
    {
      order_id: DataTypes.INTEGER,
      product_id: DataTypes.INTEGER,
      amount: DataTypes.INTEGER,
      price_unit: DataTypes.REAL
    },
    {
      getterMethods: {
        subTotal() {
          return this.price_unit * this.amount;
        }
      }
    }
  );
  OrderItem.associate = function(models) {
    OrderItem.belongsTo(models.Order, { foreignKey: "order_id", as: "items" });
  };
  return OrderItem;
};
