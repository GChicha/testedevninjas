"use strict";
module.exports = (sequelize, DataTypes) => {
  const UserToken = sequelize.define(
    "UserToken",
    {
      user_id: DataTypes.INTEGER,
      token: DataTypes.STRING,
      last_request: DataTypes.DATE
    },
    {}
  );
  UserToken.associate = function(models) {
    UserToken.belongsTo(models.User, { foreignKey: "user_id" });
  };
  return UserToken;
};
