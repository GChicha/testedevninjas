"use strict";
module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define(
    "Product",
    {
      sku: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      price: {
        type: DataTypes.REAL,
        allowNull: false
      }
    },
    {}
  );
  Product.associate = function(models) {
    Product.hasMany(models.OrderItem, { foreignKey: "product_id" });
  };
  return Product;
};
