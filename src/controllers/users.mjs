import db from "../models";
import crypto from "crypto";

export async function recoverUser(token) {
  const userToken = await db.UserToken.findOne({
    where: {
      token
    }
  });

  if (userToken) {
    userToken.last_request = new Date();
    await userToken.save();
    return userToken.user_id;
  } else return undefined;
}

export async function create(username, password) {
  const salt = crypto.randomBytes(16).toString("hex");
  const saltedPassword = password + salt;

  const hash = crypto
    .pbkdf2Sync(password, salt, 1000, 64, `sha512`)
    .toString(`hex`);

  db.User.create({
    user: username,
    password_hash: hash,
    salt
  });
}

export async function login(username, password) {
  const user = await db.User.findOne({ where: { user: username } });
  if (user) {
    const hash = crypto
      .pbkdf2Sync(password, user.salt, 1000, 64, `sha512`)
      .toString(`hex`);
    if (user.password_hash == hash) {
      const token = crypto.randomBytes(32).toString("hex");

      await db.UserToken.create({
        user_id: user.id,
        token,
        last_request: new Date()
      });

      return token;
    }
  } else return undefined;
}
