import db from "../models";

export async function byId(id) {
  return await db.Order.findByPk(id);
}

export async function list() {
  return await db.Order.findAll({
    include: [
      {
        model: db.OrderItem
      },
      {
        model: db.Customer
      }
    ]
  });
}

export async function listByStatus(status) {
  return await db.Order.findAll({
    where: {
      status
    },
    include: [
      {
        model: db.OrderItem
      },
      {
        model: db.Customer
      }
    ]
  });
}

export async function create(customer_id) {
  const customer = await db.Customer.findOne({ where: { id: customer_id } });

  if (customer)
    return await db.Order.create({
      customer_id
    });
  else throw new Error("Customer not exists");
}

export async function addItem(order_id, product_id, amount) {
  const item = await db.Product.findByPk(product_id);

  if (item) {
    await db.OrderItem.create({
      order_id,
      product_id,
      amount,
      price_unit: item.price
    });
  } else {
    throw new Error("Product not exist");
  }
}
