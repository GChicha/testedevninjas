import db from "../models";

export async function list() {
  return await db.Customer.findAll();
}

export async function add(name, cpf, email) {
  return await db.Customer.create({
    name,
    cpf,
    email
  });
}
