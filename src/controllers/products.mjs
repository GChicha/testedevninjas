import db from "../models";

export async function list() {
  return await db.Product.findAll();
}

export async function add(sku, name, price) {
  return await db.Product.create({
    sku,
    name,
    price
  });
}
