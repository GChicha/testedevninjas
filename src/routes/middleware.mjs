import * as UserController from "../controllers/users";

export async function isAuthenticated(req, res, next) {
  const token = req.get("X-Token");

  if (token) {
    const id = await UserController.recoverUser(token);
    if (id) {
      req.userId = id;
      next();
    } else {
      res.sendStatus(401);
    }
  } else res.status(400).send("Missing authentication token");
}
