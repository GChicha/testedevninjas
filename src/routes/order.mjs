import express from "express";
import * as OrderController from "../controllers/orders";

const orderRouter = express.Router();

orderRouter.get("/", async (req, res) => {
  try {
    res.status(200).send(
      (await OrderController.list()).map(order => ({
        ...order.toJSON(),
        total: order.total
      }))
    );
  } catch (e) {
    res.status(500).send(e.message);
  }
});
orderRouter.post("/:id/products", async (req, res) => {
  const { id } = req.params;
  const { product_id, amount } = req.body;

  try {
    if (product_id && amount) {
      await OrderController.addItem(id, product_id, amount);
      res.sendStatus(204);
    } else res.status(400).send("Missing product_id or amount");
  } catch (e) {
    res.status(400).send(e.message);
  }
});
orderRouter.get("/:id/products", async (req, res) => {});
orderRouter.put("/:id", async (req, res) => {
  const { id } = req.params;

  const order = await OrderController.byId(id);

  if (order) {
    order.status = "CANCELED";
    order.save();

    res.sendStatus(204);
  } else {
    res.status(400).send("Order not exist");
  }
});
orderRouter.post("/", async (req, res) => {
  const { customer_id } = req.body;

  if (!!customer_id) {
    try {
      const order = await OrderController.create(customer_id);
      return res.send({ id: order.get("id") });
    } catch (e) {
      return res.status(400).send(e.message);
    }
  } else {
    return res.status(400).send("Missing customer_id");
  }
});

export default orderRouter;
