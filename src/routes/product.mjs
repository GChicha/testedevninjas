import express from "express";
import * as ProductController from "../controllers/products";
import Sequelize from "sequelize";

const productRouter = express.Router();

productRouter.get("/", async (req, res) =>
  res.status(200).send(await ProductController.list())
);
productRouter.post("/", async (req, res) => {
  const { sku, name, price } = req.body;

  try {
    await ProductController.add(sku, name, price);
    res.sendStatus(204);
  } catch (e) {
    if (e instanceof Sequelize.ValidationError) res.sendStatus(400);
    else res.sendStatus(500);
  }
});

export default productRouter;
