import express from "express";
import * as customerController from "../controllers/customers";
import Sequelize from "sequelize";

const customerRouter = express.Router();

customerRouter.get("/", async (req, res) => {
  res.status(200).send(await customerController.list());
});
customerRouter.post("/", async (req, res) => {
  const { name, email, cpf } = req.body;

  try {
    await customerController.add(name, cpf, email);
    res.sendStatus(204);
  } catch (e) {
    if (e instanceof Sequelize.ValidationError)
      res.status(400).send(e.errors[0].message);
    else res.sendStatus(500);
  }
});

export default customerRouter;
