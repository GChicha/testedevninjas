import express from "express";
import * as userController from "../controllers/users";
import Sequelize from "sequelize";

const userRouter = express.Router();

userRouter.post("/login", async (req, res) => {
  const { username, password } = req.body;

  if (username && password) {
    const token = await userController.login(username, password);
    if (token) res.send(token);
    else res.sendStatus(401);
  } else res.status(400).send("Missing username or password");
});
userRouter.post("/", async (req, res) => {
  const { username, password } = req.body;

  if (username && password) {
    await userController.create(username, password);
    res.sendStatus(204);
  } else res.status(400).send("Missing username or password");
});

export default userRouter;
