FROM node

WORKDIR /app

COPY package-lock.json .
COPY package.json .

RUN npm install

EXPOSE 3000

VOLUME /app/data

COPY . .

ENTRYPOINT npm start
